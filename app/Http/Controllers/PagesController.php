<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index() {
        $title = 'Welcome To Laravel!';
        //return view('pages.index', compact('title'));
        return view('pages.index')->with('title', $title); /*use this method if you want to pass in
        multiple values as an array*/
    }
    public function about() {
        $title = 'This is the page for learning more about Laravel';
        return view('pages.about')->with('title', $title);
    }
    public function services() {
        $data = array(
            'title' => 'Services',
            'fakenews' => 'more fake news yo',
            'services' => ['Web Dev', 'Programming', 'SEO']
        );
        return view('pages.services')->with($data);
    }
}
